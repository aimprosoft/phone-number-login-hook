package com.aimprosoft.liferay.action.login;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.BaseStrutsPortletAction;
import com.liferay.portal.kernel.struts.StrutsPortletAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.*;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import javax.portlet.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class LoginCustomAction extends BaseStrutsPortletAction {

    private static final String LOGIN = "login";
    private static final String NUMBER = "number";
    private static final String PRIMARY = "primary";
    private static final String COMPANY_ID = "companyId";

    private static final String PHONE_NUMBER_AUTH = "phone-number-auth";

    @Override
    public void processAction(StrutsPortletAction originalStrutsPortletAction, PortletConfig portletConfig,
                              ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

        //login value from sign-in form (emailAddress,userID,screenName or phoneNumber)
        String login = ParamUtil.getString(actionRequest, LOGIN);

        //determine auth type
        Company company = PortalUtil.getCompany(actionRequest);
        String authType = company.getAuthType();

        if (CompanyConstants.AUTH_TYPE_SN.equals(authType)) {
            //screenName auth type - may be screenName (out-of-the-box) or phoneNumber (custom)

            //check if phoneNumber auth type
            boolean isPhoneNumberAuth = Boolean.TRUE.toString().equals(PrefsPropsUtil.getString(company.getCompanyId(), PHONE_NUMBER_AUTH));
            if (isPhoneNumberAuth) {

                //Portal instance companyID
                long companyId = company.getCompanyId();

                //Get user's screenName by login (phoneNumber in this case)
                String screenName = getUserScreenNameByPhoneNumber(login, companyId);

                //check is screenName is not blank
                if (!StringPool.BLANK.equals(screenName)) {
                    //substitute "login" parameter with screenName value
                    modifyRequestParam(actionRequest, LOGIN, new String[]{screenName});
                }
            }
        }

        //Process original struts action (com.liferay.portlet.login.action.LoginAction)
        //(with substituted "login" parameter in case of phoneNumber auth)
        originalStrutsPortletAction.processAction(
                originalStrutsPortletAction, portletConfig, actionRequest,
                actionResponse);
    }

    /**
     * Gets user's screenName by his primary phone number
     *
     * @param phoneNumber - user's phone number
     * @param companyId - ID of
     *
     * @return - user's screenName
     */
    private String getUserScreenNameByPhoneNumber(String phoneNumber, long companyId) {
        String screenName = StringPool.BLANK;
        try {
            //get primary phones list by phoneNumber
            DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Phone.class, PortalClassLoaderUtil.getClassLoader());
            dynamicQuery.add(RestrictionsFactoryUtil.eq(COMPANY_ID, companyId));
            dynamicQuery.add(RestrictionsFactoryUtil.eq(NUMBER, phoneNumber));
            dynamicQuery.add(RestrictionsFactoryUtil.eq(PRIMARY, Boolean.TRUE));
            List list = PhoneLocalServiceUtil.dynamicQuery(dynamicQuery);

            //check if such phone exist
            if (list != null && list.size() > 0) {
                //get phone from the list
                Phone phone = (Phone) list.get(0);
                if (Contact.class.getName().equals(phone.getClassName())) {
                    //get contact from phone
                    Contact contact = ContactLocalServiceUtil.getContact(phone.getClassPK());
                    if (User.class.getName().equals(contact.getClassName())) {
                        //get user from contact
                        User user = UserLocalServiceUtil.getUser(contact.getClassPK());
                        //finally, get user's screenName
                        screenName = user.getScreenName();
                    }
                }
            }
        } catch (Exception e) {
            _log.error(e, e);
        }
        return screenName;
    }

    /**
     * Sets request parameter to actionRequest (by means of reflection API)
     *
     * @param actionRequest - portlet action request
     * @param paramName - name of parameter to modify
     * @param paramValues - array of values
     */
    @SuppressWarnings("unchecked")
    private void modifyRequestParam(ActionRequest actionRequest, String paramName, String[] paramValues) throws IllegalAccessException {

        try {
            //Get class for ActionRequest (com.liferay.portlet.ActionRequestImpl instance)
            Class<? extends ActionRequest> actionRequestClass = actionRequest.getClass();

            Object requestObj = null; //com.liferay.portal.kernel.servlet.DynamicServletRequest instance

            //try to get "com.liferay.portlet.PortletRequestImpl._portletRequestDispatcherRequest" field first (if it's not null)
            Field _portletRequestDispatcherRequestField = findDeclaredField(actionRequestClass, "_portletRequestDispatcherRequest");
            if (_portletRequestDispatcherRequestField != null) {
                requestObj = _portletRequestDispatcherRequestField.get(actionRequest);
            }

            //otherwise, get "com.liferay.portlet.PortletRequestImpl._request" field
            if (requestObj == null) {
                Field requestField = findDeclaredField(actionRequestClass, "_request");
                if (requestField != null) {
                    requestObj = requestField.get(actionRequest);
                }
            }

            if (requestObj != null) {
                //get "com.liferay.portal.kernel.servlet.DynamicServletRequest._params" field
                Field paramsField = findDeclaredField(requestObj.getClass(), "_params");
                if (paramsField != null) {
                    //put parameter to map
                    ((Map) paramsField.get(requestObj)).put(paramName, paramValues);
                }
            }
        } catch (Exception e) {
            _log.error(e, e);
        }
    }

    /**
     * Finds field in in class and it's superclasses hierarchy
     *
     * @param targetClass - target class
     * @param name        - field name
     * @return - field
     */
    private Field findDeclaredField(Class targetClass, String name) {
        Field field = null;
        while (targetClass != null) {
            try {
                field = targetClass.getDeclaredField(name);
                if (field != null) {
                    field.setAccessible(true);
                    return field;
                }
            } catch (Exception ignore) {
                targetClass = targetClass.getSuperclass();
            }
        }
        return field;
    }

    @Override
    public String render(StrutsPortletAction originalStrutsPortletAction, PortletConfig portletConfig,
                         RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

        return originalStrutsPortletAction.render(null, portletConfig, renderRequest, renderResponse);

    }

    @Override
    public void serveResource(StrutsPortletAction originalStrutsPortletAction, PortletConfig portletConfig,
                              ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {

        originalStrutsPortletAction.serveResource(originalStrutsPortletAction, portletConfig, resourceRequest, resourceResponse);
    }

    private static Log _log = LogFactoryUtil.getLog(LoginCustomAction.class);
}
