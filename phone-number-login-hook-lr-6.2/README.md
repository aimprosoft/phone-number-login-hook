#Phone number Login Hook

##Introduction

Phone number Login Hook plugin is developed by Vitaliy Koshelenko and allows Liferay users to sign in using their primary phone number.

Plugins adds additional Authentication type to portal settings, called "By Phone Number" 
(in addition to existing "By Email Address", "By Screen Name", "By User ID"). 
Once this authentication type is selected in portal settings - "Phone Number" and "Password" fields are displayed for users 
in Sign In portlet.

##Functionality

### Additional authentication type in portal settings: 

![Portal-Settings](screens/portal-settings.jpg)

### Sign In portlet:

![Sign-In-Portlet](screens/sign-in-portlet.jpg)
